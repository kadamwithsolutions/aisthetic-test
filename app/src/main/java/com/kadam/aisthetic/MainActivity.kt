package com.kadam.aisthetic

import android.os.Bundle
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kadam.aisthetic.adapter.ItemAdapter
import com.kadam.aisthetic.adapter.ItemAdapterListener
import com.kadam.aisthetic.adapter.OnItemSelectedListener
import com.kadam.aisthetic.model.Item
import com.kadam.aisthetic.util.Constants

class MainActivity : AppCompatActivity(), ItemAdapterListener,
    OnItemSelectedListener.OnItemSelected {
    var position: Int = -1
    lateinit var recyclerview: RecyclerView
    lateinit var items: ArrayList<Item>
    lateinit var tvTitle: TextView
    lateinit var tvDes: TextView
    lateinit var content: LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvTitle = findViewById(R.id.tv_title)
        tvDes = findViewById(R.id.tv_des)
        content = findViewById(R.id.content)
        items = getItems();
        recyclerview = findViewById(R.id.recycler)
        recyclerview.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        recyclerview.adapter = ItemAdapter(this, items)

        recyclerview.addOnScrollListener(OnItemSelectedListener(object :
            OnItemSelectedListener.OnItemSelected {
            override fun onSelected(position: Int) {
                updateItemDetails(items[position])
            }
        }))
    }

    @JvmName("getItems1")
    private fun getItems(): ArrayList<Item> {
        val items = ArrayList<Item>()
        items.add(
            Item(
                "Christopher Avery",
                "Taking responsibility - practicing 100 percent responsibility every day - is about seeing ourselves not as right or wrong, but as an agent, chooser, problem solver, and learner in the complex interrelationships of our lives so that we can better integrate with the people and world around us. When we do this, we enjoy a better and more productive way to live and lead.",
                R.raw.vid_01,
                Constants.TYPE_VIDEO
            )
        )
        items.add(
            Item(
                "Linda Breen Pierce",
                "Simplicity requires a two-step process. First, we must invest time and energy to discover what stirs us as human beings, what makes our hearts sing, and what brings us joy. Then, we must proceed to create the life that reflects the unique people we truly are. This is the heart and soul of simplicity",
                R.raw.img_01,
                Constants.TYPE_IMAGE
            )
        )
        items.add(
            Item(
                "Jim Cathcart",
                "We have three roles here on earth: to learn, to love, and to live. When we stop learning, we start to stagnate and die. When we stop loving, we lose our sense of purpose and become self-centered. When we limit our living, we deny the world the benefits of our talents",
                R.raw.img_02,
                Constants.TYPE_IMAGE
            )
        )
        items.add(
            Item(
                "Michael Jeans",
                "A life portfolio offers a compelling alternative to traditional retirement. It is a new way of thinking and living in extended middle age. A typical portfolio is a balanced mix of some work, ongoing learning, recreation, travel and avocations, reconnecting with family and friends, and giving back.",
                R.raw.img_03,
                Constants.TYPE_IMAGE
            )
        )
        items.add(
            Item(
                "T-Ralph Olaniyi",
                "Most of those who have succeeded in life can trace their success back to the essential education they obtained from parents, teachers and/ or friends.",
                R.raw.img_04,
                Constants.TYPE_IMAGE
            )
        )
        items.add(
            Item(
                "Zig Ziglar",
                "The foundation stones of honesty, character, faith, integrity, love, and loyalty are necessary for a balanced success that includes health, wealth, and happiness. As you go onward and upward in life, you will discover that if you compromise any of these principles you will end up with only a beggar's portion of what life has to offe",
                R.raw.img_05,
                Constants.TYPE_IMAGE
            )
        )
        items.add(
            Item(
                "Squire Rushnell",
                "You - yourself, not someone else - need to determine where you want to go and what you want to do in your life. And once you make that decision, you can begin mapping a plan to get there and focusing on that objective every single day",
                R.raw.img_06,
                Constants.TYPE_IMAGE
            )
        )
        return items
    }


    private fun updateItemDetails(item: Item) {
        Log.d("Anim", item.title)

        val fadedownAnim = AnimationUtils.loadAnimation(this, R.anim.fade_down)
        val fadeUpAnim = AnimationUtils.loadAnimation(this, R.anim.fade_up)
        content.clearAnimation()
        fadedownAnim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                tvTitle.text = item.title
                tvDes.text = item.des
                content.animation = fadeUpAnim
                content.animate()
            }

            override fun onAnimationRepeat(animation: Animation?) {
            }
        })
        content.animation = fadedownAnim
        content.animate()
        // tvDes.startAnimation(fadedownAnim)

    }

    override fun getSelectedItem(): Int {
        return position
    }

    override fun onSelected(position: Int) {
        updateItemDetails(items[position])
    }

}
