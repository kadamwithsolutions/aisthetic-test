package com.kadam.aisthetic.util

import android.content.Context
import android.net.Uri


class Utils {

    companion object {
        fun getURI(context: Context, raw: Int): Uri {
            return Uri.parse(
                "android.resource://" +
                        context.packageName +
                        "/" + raw
            )
        }
    }

}
