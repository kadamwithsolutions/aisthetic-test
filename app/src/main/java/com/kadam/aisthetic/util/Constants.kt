package com.kadam.aisthetic.util

class Constants {
    companion object {
        val TYPE_IMAGE = 0
        val TYPE_VIDEO = 1
    }
}