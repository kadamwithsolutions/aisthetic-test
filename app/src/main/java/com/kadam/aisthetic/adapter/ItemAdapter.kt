package com.kadam.aisthetic.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.VideoView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.kadam.aisthetic.R
import com.kadam.aisthetic.model.Item
import com.kadam.aisthetic.util.Constants
import com.kadam.aisthetic.util.Utils

class ItemAdapter(val listner: ItemAdapterListener, val items: ArrayList<Item>) :
    RecyclerView.Adapter<ItemAdapter.VH>() {


    abstract class VH(val listner: ItemAdapterListener, itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var item: Item

        abstract fun setModel(item: Item)
        abstract fun onDetached()
        abstract fun onAttached()
    }

    class ImageVH(listner: ItemAdapterListener, itemView: View) : VH(listner, itemView) {
        var ivImage: AppCompatImageView = itemView.findViewById(R.id.iv_media)

        override fun setModel(item: Item) {
            ivImage.load(item.uri)
        }

        override fun onDetached() {

        }

        override fun onAttached() {

        }

    }

    class VideoVH(listner: ItemAdapterListener, itemView: View) : VH(listner, itemView) {
        var vvVideo: VideoView = itemView.findViewById(R.id.vv_media)

        override fun setModel(item: Item) {
            this.item = item
        }

        private fun prepare(uri: Int) {
            vvVideo.setVideoURI(Utils.getURI(itemView.context, uri))
            vvVideo.setOnPreparedListener { it.isLooping = true }
        }

        override fun onDetached() {
            Log.d("Player", "Detached")

            if (vvVideo.isPlaying) {
                vvVideo.pause()
            }
        }

        override fun onAttached() {
            Log.d("Player", "Attached:" + vvVideo.isPlaying)
            if (!vvVideo.isPlaying) {
                prepare(item.uri)
                vvVideo.start()
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return items.get(position).type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return if (viewType == Constants.TYPE_IMAGE) {
            ImageVH(
                listner,
                LayoutInflater.from(parent.context).inflate(R.layout.row_image, parent, false)
            )
        } else {
            VideoVH(
                listner,
                LayoutInflater.from(parent.context).inflate(R.layout.row_video, parent, false)
            )

        }
    }


    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.setModel(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }
}