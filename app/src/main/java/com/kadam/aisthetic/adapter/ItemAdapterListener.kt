package com.kadam.aisthetic.adapter

interface ItemAdapterListener {
    fun getSelectedItem(): Int
}