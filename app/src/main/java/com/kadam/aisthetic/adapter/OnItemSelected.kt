package com.kadam.aisthetic.adapter

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class OnItemSelectedListener(val mListner: OnItemSelected) : RecyclerView.OnScrollListener() {
    var position: Int = -1

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val manager = recyclerView.layoutManager as LinearLayoutManager
        if (position != manager.findFirstCompletelyVisibleItemPosition()) {
            position = manager.findFirstCompletelyVisibleItemPosition()
            Log.d(
                "Position First:",
                manager.findFirstVisibleItemPosition().toString()
            )
            Log.d(
                "Position Last:",
                manager.findLastVisibleItemPosition().toString()
            )
            val firstVisible = manager.findFirstVisibleItemPosition()
            val lastVisible = manager.findLastVisibleItemPosition()

            if (position != -1) {
                if (firstVisible != position)
                    (recyclerView.findViewHolderForLayoutPosition(firstVisible) as ItemAdapter.VH).onDetached()

                if (lastVisible != position)
                    (recyclerView.findViewHolderForLayoutPosition(lastVisible) as ItemAdapter.VH).onDetached()

                (recyclerView.findViewHolderForLayoutPosition(position) as ItemAdapter.VH).onAttached()

                mListner.onSelected(position)
            }
        }
    }

    public interface OnItemSelected {
        fun onSelected(position: Int)
    }

}